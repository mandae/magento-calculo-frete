<?php
/**
 * Mandaê
 *
 * @category   Mandae
 * @package    Mandae_Shipping
 * @author     Thiago Contardi
 * @copyright  Copyright (c) 2017 Bizcommerce
 * @author     Bruno Ferreira
 * @copyright  Copyright (c) 2019 Cloud Online
 * @author     João Melo <joao@cloudonline.com.br>
 * @copyright  Copyright (c) 2019 Mandaê
 */
class Mandae_Shipping_Model_Api
{
    const URL_API = 'https://api.mandae.com.br/v3/';

    const SANDBOX_URL_API = 'https://sandbox.api.mandae.com.br/v3/';

    protected $_helper;

    /**
     * @author João Melo <joao@cloudonline.com.br>
     * @param $data
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     */
    public function shippingRate($data, Mandae_Shipping_Model_Carrier_Mandae $carrier)
    {
        $postcode = $data['postcode'];
        $url = $this->_getUrl() . 'postalcodes/' . $postcode . '/rates';
        $args = ['items' => []];

        foreach($data['items'] as $item) {
            $newItem = [
                'height' => $item['height'],
                'length' => $item['length'],
                'quantity' => $item['quantity'],
                'weight' => $item['weight'],
                'width' => $item['width'],
            ];

            if (Mage::getStoreConfig('carriers/mandae/weight_type') == 'g')
                $newItem['weight'] = round($newItem['weight'] / 1000, 3, PHP_ROUND_HALF_UP);

            if (isset($item['declaredValue']))
                $newItem['declaredValue'] = $item['declaredValue'];

            $args['items'][] = $newItem;
        }

        return $this->_request($carrier, $url, 'POST', $args);
    }

    /**
     *
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     * @return mixed
     */
    public function tracking(Mandae_Shipping_Model_Carrier_Mandae $carrier, $trackingCode)
    {
        $url = $this->_getUrl() . 'trackings/' . $trackingCode;
        return $this->_request($carrier, $url);
    }

    /**
     * Executa uma requisição HTTP
     * 
     * @author João Melo <joao@cloudonline.com.br>
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     * @param string $url
     * @param string $method
     * @param array $data
     *
     * @return mixed
     */
    protected function _request(Mandae_Shipping_Model_Carrier_Mandae $carrier, $url, $method = 'GET', $data = null)
    {
        /** @var Mandae_Shipping_Helper_Data $helper */
        $helper = Mage::helper('mandae');
        $helper->log("API REQUEST: $method $url");
        $helper->log('API TOKEN: ' . substr($carrier->getConfigData('token'), 0, 5) . ' (first 5)');

        $options = array(
            'http' => array(
                'header' => sprintf("Authorization: %s\r\n", $carrier->getConfigData('token')),
                'method' => $method,
            ),
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
            ),
        );

        if ($data !== null) {
            $options['http']['content'] = json_encode($data);
            $options['http']['header'] .= "Content-Type: application/json\r\n";
            $helper->log('API REQUEST BODY: ' . json_encode($data, JSON_PRETTY_PRINT));
        }

        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $helper->log('API RESPONSE: ' . json_encode(json_decode($response), JSON_PRETTY_PRINT));

        return ($response === false) ? false : json_decode($response);
    }

    protected function _getUrl()
    {
        /** @var Mandae_Shipping_Helper_Data $helper */
        $helper = Mage::helper('mandae');
        return $helper->isSandbox() ? self::SANDBOX_URL_API : self::URL_API;
    }
}
