<?php

/**
 * Mandaê
 *
 * @category   Mandae
 * @package    Mandae_Shipping
 * @author     Thiago Contardi
 * @author     Bruno Rodrigues Ferreira
 * @author     João Melo <joao@cloudonline.com.br>
 * @copyright  Copyright (c) 2017 Bizcommerce
 * @copyright  Copyright (c) 2018 Mandaê
 */
class Mandae_Shipping_Model_Source_Services
{
    public function toOptionArray()
    {
        $options = Mage::helper('mandae')->getAllowedMethods();
        $result = array();
        
        foreach ($options as $key => $option) {
            $result[] = array(
                'value' => $key,
                'label' => $option
            );
        }

        return $result;
    }
}