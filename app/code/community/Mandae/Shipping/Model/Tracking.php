<?php
/**
 * Mandaê Shipping Tracking
 * 
 * @author    João Melo <joao@cloudonline.com.br>
 * @category  Mandae
 * @copyright © Cloud Online 2020
 * @package   Mandae_Shipping
*/
class Mandae_Shipping_Model_Tracking
{
    /**
     * Get tracking history
     * 
     * @param string $trackingCode
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     * @return boolean
    */
    public function getTracking($trackingCode, Mandae_Shipping_Model_Carrier_Mandae $carrier)
    {
        /** @var Mandae_Shipping_Helper_Data $helper */
        $helper = Mage::helper('mandae');

        try {
            /** @var Mandae_Shipping_Model_Api $api */
            $api = Mage::getModel('mandae/api');
            $response = $api->tracking($carrier, $trackingCode);
#print_r($response);exit;
            if (!$response)
                throw new Exception('TRACKING API: Resposta inválida');

            if (isset($response->error->message))
                throw new Exception('TRACKING API: ' . $response->error->message);
            
            $carrierTitle = $carrier->getConfigData('title');

            if (isset($response->carrierName))
                $carrierTitle .= " ({$response->carrierName})";
            
            $baseUrl = ($helper->isSandbox() ? 'sandbox.app' : 'app') . '.mandae.com.br';

            $data = array(
                'url' => "https://$baseUrl/rastreamento/$trackingCode",
                'progressdetail' => array()
            );

            if (isset($response->events)) {
                foreach ($response->events as $event) {
                    $ts = strtotime($event->timestamp);

                    $data['progressdetail'][] = array(
                        'deliverydate' => date('Y-m-d', $ts),
                        'deliverytime' => date('H:i:s', $ts),
                        'deliverylocation' => $event->name,
                        'activity' => $event->description
                    );
                }
            }

            /** @var Mage_Shipping_Model_Tracking_Result_Status $tracking */
            $tracking = Mage::getModel('shipping/tracking_result_status');
            $tracking->setTracking($trackingCode);
            $tracking->setCarrier($carrier->getCarrierCode());
            $tracking->setCarrierTitle($carrierTitle);
            $tracking->addData($data);
            $carrier->getResult()->append($tracking);
            return true;
        } catch (Exception $e) {
            $helper->log($e->getMessage(), Zend_Log::ERR);
            /** @var Mage_Shipping_Model_Tracking_Result_Error $tracking */
            $tracking = Mage::getModel('shipping/tracking_result_error');
            $tracking->setTracking($trackingCode);
            $tracking->setCarrier($carrier->getCarrierCode());
            $tracking->setCarrierTitle($carrier->getConfigData('title'));
            $carrier->getResult()->append($tracking);
            return false;
        }
    }
}
